package com.quovadis.contentsearch.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Objects;

@Document(collection = "content")
public class ContentDocument {
    @Id
    public ObjectId _id;
    String content;
    String title;
    String author;
    String description;
    String origin;
    String url;
    Date createdDate;

    public ContentDocument(String content, String title, String author, String description, String origin, String url, Date createdDate) {
        this._id = new ObjectId();
        this.content = content;
        this.title = title;
        this.author = author;
        this.description = description;
        this.origin = origin;
        this.url = url;
        this.createdDate = createdDate;
    }

    //Getters and setters
    // ObjectId needs to be converted to string
    public String get_id() { return _id.toHexString(); }
    public void set_id(ObjectId _id) { this._id = _id; }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContentDocument that = (ContentDocument) o;
        return Objects.equals(_id, that._id) &&
                Objects.equals(content, that.content) &&
                Objects.equals(title, that.title) &&
                Objects.equals(author, that.author) &&
                Objects.equals(description, that.description) &&
                Objects.equals(origin, that.origin) &&
                Objects.equals(url, that.url) &&
                Objects.equals(createdDate, that.createdDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(_id, content, title, author, description, origin, url, createdDate);
    }

    @Override
    public String toString() {
        return "ContentDocument{" +
                "_id=" + _id +
                ", content='" + content + '\'' +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", description='" + description + '\'' +
                ", origin='" + origin + '\'' +
                ", url='" + url + '\'' +
                ", createdDate=" + createdDate +
                '}';
    }
}
