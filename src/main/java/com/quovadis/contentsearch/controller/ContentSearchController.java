package com.quovadis.contentsearch.controller;

import com.quovadis.contentsearch.model.ContentDocument;
import com.quovadis.contentsearch.service.ContentSearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.spring5.context.webflux.ReactiveDataDriverContextVariable;
import reactor.core.publisher.Flux;

import java.time.Duration;

@RestController
@RequestMapping(path = "/v1/contentsearch")
public class ContentSearchController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContentSearchController.class);

    @Autowired
    ContentSearchService contentSearchService;

    @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<ContentDocument> searchContent(@RequestParam(required = false) String query){
        LOGGER.info("searchContent: {}", query);
        Flux<ContentDocument> result = contentSearchService.searchContent(query);

        return result.delayElements(Duration.ofMillis(1l));
    }
}
