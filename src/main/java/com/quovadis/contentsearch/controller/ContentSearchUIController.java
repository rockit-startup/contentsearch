package com.quovadis.contentsearch.controller;

import com.quovadis.contentsearch.model.ContentDocument;
import com.quovadis.contentsearch.service.ContentSearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.spring5.context.webflux.ReactiveDataDriverContextVariable;
import reactor.core.publisher.Flux;

import java.time.Duration;

@Controller
@RequestMapping(path = "/v1/contentsearch")
public class ContentSearchUIController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContentSearchController.class);

    @Autowired
    ContentSearchService contentSearchService;

    @GetMapping("/contents")
    public String searchContentAndView(Model model, @RequestParam(required = false) String query){
        Flux<ContentDocument> result = contentSearchService.searchContent(query).delayElements(Duration.ofMillis(1l));
        result.subscribe(System.out::println);
        model.addAttribute("contents", new ReactiveDataDriverContextVariable(result, 1));
        return "contents";
    }
}

