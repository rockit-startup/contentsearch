package com.quovadis.contentsearch.external.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
public class PropertyApiExternal {
    @Value("${externals.apis}")
    public String[] apis;

    @Value("${youtube.base.url}")
    String youtubeBaseUrl;

    @Value("${youtube.bearer.token}")
    String youtubeBearerToken;

    @Value("${youtube.search.uri}")
    String youtubeSearchUri;

    @Value("${youtube.watch}")
    String youtubeWatch;

    /*
    Spotify Properties
     */

    @Value("${spotify.base.url}")
    String spotifyBaseUrl;

    @Value("${spotify.token.uri}")
    String spotifyTokenUri;

    @Value("${spotify.authorization}")
    String spotifyAuthorization;

    @Value("${spotify.search.uri}")
    String spotifySearchUri;

    /*
    TikTok
     */
    @Value("${tiktok.base.url}")
    String tiktokBaseUrl;

    @Value("${tiktok.header.key}")
    String tiktokHeaderKey;

    @Value("${tiktok.header.host}")
    String tiktokHeaderHost;

    @Value("${tiktok.search.uri}")
    String tiktokSearchUri;

    @Value("${tiktok.watch}")
    String tiktokWatch;

    public String[] getApis() {
        return apis;
    }

    public void setApis(String[] apis) {
        this.apis = apis;
    }

    public String getYoutubeBaseUrl() {
        return youtubeBaseUrl;
    }

    public void setYoutubeBaseUrl(String youtubeBaseUrl) {
        this.youtubeBaseUrl = youtubeBaseUrl;
    }

    public String getYoutubeBearerToken() {
        return youtubeBearerToken;
    }

    public void setYoutubeBearerToken(String youtubeBearerToken) {
        this.youtubeBearerToken = youtubeBearerToken;
    }

    public String getYoutubeSearchUri() {
        return youtubeSearchUri;
    }

    public void setYoutubeSearchUri(String youtubeSearchUri) {
        this.youtubeSearchUri = youtubeSearchUri;
    }

    public String getYoutubeWatch() {
        return youtubeWatch;
    }

    public void setYoutubeWatch(String youtubeWatch) {
        this.youtubeWatch = youtubeWatch;
    }

    public String getSpotifyBaseUrl() {
        return spotifyBaseUrl;
    }

    public void setSpotifyBaseUrl(String spotifyBaseUrl) {
        this.spotifyBaseUrl = spotifyBaseUrl;
    }

    public String getTiktokHeaderKey() {
        return tiktokHeaderKey;
    }

    public void setTiktokHeaderKey(String tiktokHeaderKey) {
        this.tiktokHeaderKey = tiktokHeaderKey;
    }

    public String getTiktokHeaderHost() {
        return tiktokHeaderHost;
    }

    public void setTiktokHeaderHost(String tiktokHeaderHost) {
        this.tiktokHeaderHost = tiktokHeaderHost;
    }

    public String getSpotifyTokenUri() {
        return spotifyTokenUri;
    }

    public void setSpotifyTokenUri(String spotifyTokenUri) {
        this.spotifyTokenUri = spotifyTokenUri;
    }

    public String getSpotifyAuthorization() {
        return spotifyAuthorization;
    }

    public void setSpotifyAuthorization(String spotifyAuthorization) {
        this.spotifyAuthorization = spotifyAuthorization;
    }

    public String getSpotifySearchUri() {
        return spotifySearchUri;
    }

    public void setSpotifySearchUri(String spotifySearchUri) {
        this.spotifySearchUri = spotifySearchUri;
    }

    public String getTiktokBaseUrl() {
        return tiktokBaseUrl;
    }

    public void setTiktokBaseUrl(String tiktokBaseUrl) {
        this.tiktokBaseUrl = tiktokBaseUrl;
    }

    public String getTiktokSearchUri() {
        return tiktokSearchUri;
    }

    public void setTiktokSearchUri(String tiktokSearchUri) {
        this.tiktokSearchUri = tiktokSearchUri;
    }

    public String getTiktokWatch() {
        return tiktokWatch;
    }

    public void setTiktokWatch(String tiktokWatch) {
        this.tiktokWatch = tiktokWatch;
    }
}