package com.quovadis.contentsearch.external.service.imp;

import com.quovadis.contentsearch.external.configuration.PropertyApiExternal;
import com.quovadis.contentsearch.external.facade.ApiExternalContentFacade;
import com.quovadis.contentsearch.external.model.ContentApiExternalResponse;
import com.quovadis.contentsearch.external.service.ContentSearchExternalService;
import com.quovadis.contentsearch.model.ContentDocument;
import com.quovadis.contentsearch.repository.ContentSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.Date;

@Service
public class ContentSearchExternalServiceImp implements ContentSearchExternalService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContentSearchExternalServiceImp.class);
    ApiExternalContentFacade apiExternalContentFacade;

    @Autowired
    ContentSearchRepository contentSearchRepository;

    public ContentSearchExternalServiceImp (WebClient webClient, PropertyApiExternal propertyApiExternal){
        this.apiExternalContentFacade = new ApiExternalContentFacade(webClient, propertyApiExternal);
    }

    @Override
    public Flux<ContentDocument> findExternalContent(String query) {
        return apiExternalContentFacade
                .getAllContentApiExternal(query)
                .map(contentApiExternalResponse -> contentExternalToContent(contentApiExternalResponse, query))
                .flatMap(content -> contentSearchRepository.save(content));
    }

    private ContentDocument contentExternalToContent(ContentApiExternalResponse contentApiExternalResponse, String query) {
        LOGGER.info("contentExternalToContent");
        LOGGER.info(contentApiExternalResponse.toString());

        //Transform ContentExternal To Content
        return new ContentDocument(
                query,
                contentApiExternalResponse.getTitle(),
                contentApiExternalResponse.getAuthor(),
                contentApiExternalResponse.getDescription(),
                contentApiExternalResponse.getApiExternal(),
                contentApiExternalResponse.getUrl(),
                new Date());
    }
}
