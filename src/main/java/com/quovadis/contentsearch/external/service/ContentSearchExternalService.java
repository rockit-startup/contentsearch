package com.quovadis.contentsearch.external.service;

import com.quovadis.contentsearch.model.ContentDocument;
import reactor.core.publisher.Flux;

public interface ContentSearchExternalService {
    Flux<ContentDocument> findExternalContent(String query);
}
