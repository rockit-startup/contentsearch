package com.quovadis.contentsearch.external.facade.spotify.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class AllContentResponse {
    ContentSearchResponse tracks;

    public ContentSearchResponse getTracks() {
        return tracks;
    }

    public void setTracks(ContentSearchResponse tracks) {
        this.tracks = tracks;
    }

    @Override
    public String toString() {
        return "AllContentResponse{" +
                "track=" + tracks +
                '}';
    }
}
