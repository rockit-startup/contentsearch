package com.quovadis.contentsearch.external.facade;

import com.quovadis.contentsearch.external.configuration.PropertyApiExternal;
import com.quovadis.contentsearch.external.model.ContentApiExternalResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Configuration
public class ApiExternalContentFacade {

    List<ApiExternalContentProvider> providers;

    public ApiExternalContentFacade(WebClient webClient, PropertyApiExternal propertyApiExternal) {
        loadProviders(propertyApiExternal, webClient);
        this.setUpAllContentApiExternal();
    }

    private void loadProviders(PropertyApiExternal propertyApiExternal, WebClient webClient){
        String[] apis = propertyApiExternal.getApis();
        providers = new ArrayList<>();
        for (String api : apis) {
            Class apiClass = null;
            try {
                apiClass = Class.forName(api);
                Object object = apiClass.newInstance();
                Field fieldPropertyWebClient = object.getClass().getDeclaredField("webClient");

                // Es necesario establecer esta propiedad para acceder a los campos
                // de la clase directamente.
                fieldPropertyWebClient.setAccessible(true);
                fieldPropertyWebClient.set(object, webClient);

                Field fieldPropertyApiExternal = object.getClass().getDeclaredField("propertyApiExternal");

                // Es necesario establecer esta propiedad para acceder a los campos
                // de la clase directamente.
                fieldPropertyApiExternal.setAccessible(true);
                fieldPropertyApiExternal.set(object, propertyApiExternal);

                providers.add((ApiExternalContentProvider) object);
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
    }

    private void setUpAllContentApiExternal(){
        this.providers.forEach(provider->{
            provider.action(null, ApiExternalContentProvider.Action.SET_UP);
        });
    }


    public Flux<ContentApiExternalResponse> getAllContentApiExternal(String query) {
        return makeActions(providers, query, ApiExternalContentProvider.Action.GET);
    }

    private static Flux<ContentApiExternalResponse> makeActions(Collection<ApiExternalContentProvider> providers,
                                                                String query,
                                                                ApiExternalContentProvider.Action... actions) {
        var response = new AtomicReference<Flux<ContentApiExternalResponse>>(Flux.empty());
        providers.forEach(provider->{
            response.accumulateAndGet(provider.action(query, actions),(codeApiExternalResponseFlux, codeApiExternalResponseFlux2) -> codeApiExternalResponseFlux.mergeWith(codeApiExternalResponseFlux2));
        });
        return response.get();
    }
}
