package com.quovadis.contentsearch.external.facade.tiktok.model;

public class Author {
    String id;
    String uniqueId;
    String nickname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public String toString() {
        return "Author{" +
                "id='" + id + '\'' +
                ", uniqueId='" + uniqueId + '\'' +
                ", nickname='" + nickname + '\'' +
                '}';
    }
}
