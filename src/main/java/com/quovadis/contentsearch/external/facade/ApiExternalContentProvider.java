package com.quovadis.contentsearch.external.facade;

import com.quovadis.contentsearch.external.model.ContentApiExternalResponse;
import reactor.core.publisher.Flux;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;

public abstract class ApiExternalContentProvider {

    private Flux<ContentApiExternalResponse> action(Action action, String param) {
        if (action == Action.GET) {
            return get(param);
        } else if (action == Action.SET_UP){
            setUp();
            return Flux.empty();
        } else{//LOGGER.info("Undefined action");
            return Flux.empty();
        }
    }

    public Flux<ContentApiExternalResponse> action( String param, Action... actions) {
        var codeExternal = new AtomicReference<Flux<ContentApiExternalResponse>>(Flux.just());

        Arrays.stream(actions).forEach(action -> {
            codeExternal.set(this.action(action, param));
        });
        return codeExternal.get();
    }

    public abstract void setUp();

    public abstract Flux<ContentApiExternalResponse> get(String param);

    public abstract String name();

    enum Action {
        SET_UP, GET
    }
}
