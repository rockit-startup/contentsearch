package com.quovadis.contentsearch.external.facade.tiktok;

import com.quovadis.contentsearch.external.configuration.PropertyApiExternal;
import com.quovadis.contentsearch.external.facade.ApiExternalContentProvider;
import com.quovadis.contentsearch.external.facade.tiktok.model.AllContentResponse;
import com.quovadis.contentsearch.external.facade.tiktok.model.MediaTiktok;
import com.quovadis.contentsearch.external.model.ContentApiExternalResponse;
import io.netty.util.internal.StringUtil;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.stream.Collectors;

public class TiktokExternalContentWorker extends ApiExternalContentProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(TiktokExternalContentWorker.class);

    final String API_EXTERNAL_NAME = "tiktok";

    @Autowired
    PropertyApiExternal propertyApiExternal;

    WebClient webClient;

    public TiktokExternalContentWorker() {
        super();
    }

    @Override
    public void setUp() {
        this.webClient = webClient
                .mutate()
                .baseUrl(propertyApiExternal.getTiktokBaseUrl())
                .defaultHeaders(httpHeaders -> {
                    httpHeaders.add("x-rapidapi-key", propertyApiExternal.getTiktokHeaderKey());
                    httpHeaders.add("x-rapidapi-host", propertyApiExternal.getTiktokHeaderHost());
                })
                .build();
    }

    @Override
    public Flux<ContentApiExternalResponse> get(String param) {
        LOGGER.info("TiktokExternalContentWorker get()");
        String searchUri = propertyApiExternal.getTiktokSearchUri();
        searchUri = searchUri.concat("name=").concat(param);
        LOGGER.info("TiktokExternalContentWorker get(): {}", searchUri);

        LOGGER.info(webClient.head().toString());

        Flux<ContentApiExternalResponse> response = (Flux<ContentApiExternalResponse>) webClient
                .method(HttpMethod.GET)
                .uri(searchUri)
                .retrieve()
                .bodyToMono(AllContentResponse.class)
                .flatMapMany(this::convertMonoToFlux)
                .take(20);
        
        return response;
    }

    private Publisher<?> convertMonoToFlux(AllContentResponse allContentResponse) {
        return Flux
                .fromIterable(allContentResponse
                        .getMediaList()
                        .stream()
                        .map(this::convertToContentApiExternal)
                        .collect(Collectors.toList())
                );
    }

    private ContentApiExternalResponse convertToContentApiExternal(MediaTiktok mediaTiktok) {
        return new ContentApiExternalResponse(
                mediaTiktok.getAuthor().getNickname(),
                StringUtil.EMPTY_STRING,
                mediaTiktok.getDescription(),
                propertyApiExternal.getTiktokWatch().concat(mediaTiktok.getAuthor().getUniqueId()).concat("/video/").concat(mediaTiktok.getVideoId()),
                name());
    }

    @Override
    public String name() {
        return API_EXTERNAL_NAME;
    }
}
