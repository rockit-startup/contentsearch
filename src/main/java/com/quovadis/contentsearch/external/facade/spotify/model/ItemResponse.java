package com.quovadis.contentsearch.external.facade.spotify.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonAutoDetect
public class ItemResponse {
    String name;
    int popularity;

    @JsonProperty("artists")
    List<Artist> artists;

    @JsonProperty("disc_number")
    int discNumber;

    String uri;

    @JsonProperty("external_urls")
    ExternalUrls externalUrls;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPopularity() {
        return popularity;
    }

    public void setPopularity(int popularity) {
        this.popularity = popularity;
    }

    public List<Artist> getArtists() {
        return artists;
    }

    public void setArtists(List<Artist> artists) {
        this.artists = artists;
    }

    public int getDiscNumber() {
        return discNumber;
    }

    public void setDiscNumber(int discNumber) {
        this.discNumber = discNumber;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public ExternalUrls getExternalUrls() {
        return externalUrls;
    }

    public void setExternalUrls(ExternalUrls externalUrls) {
        this.externalUrls = externalUrls;
    }

    @Override
    public String toString() {
        return "ItemResponse{" +
                "name='" + name + '\'' +
                ", popularity=" + popularity +
                ", artists=" + artists +
                ", discNumber=" + discNumber +
                ", uri='" + uri + '\'' +
                ", externalUrls=" + externalUrls +
                '}';
    }
}
