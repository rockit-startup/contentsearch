package com.quovadis.contentsearch.external.facade.spotify;

import com.quovadis.contentsearch.external.configuration.PropertyApiExternal;
import com.quovadis.contentsearch.external.facade.ApiExternalContentProvider;
import com.quovadis.contentsearch.external.facade.spotify.model.AllContentResponse;
import com.quovadis.contentsearch.external.facade.spotify.model.ItemResponse;
import com.quovadis.contentsearch.external.facade.spotify.model.TokenResponse;
import com.quovadis.contentsearch.external.model.ContentApiExternalResponse;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Comparator;
import java.util.stream.Collectors;

public class SpotifyExternalContentWorker extends ApiExternalContentProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(SpotifyExternalContentWorker.class);

    final String API_EXTERNAL_NAME = "spotify";

    @Autowired
    PropertyApiExternal propertyApiExternal;

    WebClient webClient;

    String accessToken;

    //Change code
    String authorization = "MjFjODVkYzQwOWY1NDJhZjkwOTA1ZjI5NjRkMWI2ZmQ6OWE1YmNkYjE3NGE2NDcyM2I4ZjJhMDRkNGRkNWNmMjU=";

    public SpotifyExternalContentWorker(){
        super();
        LOGGER.info("SpotifyExternalContentWorker() default");
    }

    @Override
    public void setUp() {
        LOGGER.info("SpotifyExternalContentWorker() WebClient");
        this.webClient = webClient
                .mutate()
                .baseUrl(propertyApiExternal.getSpotifyBaseUrl())
                .build();

        if(accessToken == null || accessToken.isEmpty()){
            this.getToken();
        }
    }

    private void getToken() {
        Mono<TokenResponse> response = webClient
                .method(HttpMethod.POST)
                .uri(propertyApiExternal.getSpotifyTokenUri())
                .body(BodyInserters
                        .fromFormData("grant_type", "client_credentials"))
                .header("Authorization", "Basic ".concat(authorization))
                .retrieve()
                .bodyToMono(TokenResponse.class)
                .onErrorReturn(new TokenResponse());

        response.subscribe();
        accessToken = response.block().getAccess_token();
    }


    @Override
    public Flux<ContentApiExternalResponse> get(String param) {
        LOGGER.info("SpotifyExternalContentWorker get()");
        String searchUri = propertyApiExternal.getSpotifySearchUri();
        searchUri = searchUri.concat("search?type=track");
        searchUri = searchUri.concat("&q=").concat(param);

        LOGGER.info("SpotifyExternalContentWorker get(): {}", searchUri);

        Flux<ContentApiExternalResponse> response = (Flux<ContentApiExternalResponse>) webClient
                .method(HttpMethod.GET)
                .uri(searchUri)
                .header("Authorization", "Bearer ".concat(accessToken))
                .retrieve()
                .bodyToMono(AllContentResponse.class)
                .onErrorReturn(new AllContentResponse())
                .flatMapMany(this::convertMonoToFlux)
                .delayElements(Duration.ofMillis(10l));

        return response;
    }

    private Publisher<?> convertMonoToFlux(AllContentResponse allContentResponse) {
        return Flux
                .fromIterable(allContentResponse
                        .getTracks()
                        .getItems()
                        .stream()
                        .sorted(Comparator.comparingInt(ItemResponse::getPopularity).reversed())
                        .map(this::convertToContentApiExternal)
                        .collect(Collectors.toList()));
    }

    private ContentApiExternalResponse convertToContentApiExternal(ItemResponse itemResponse) {
        return new ContentApiExternalResponse(
                itemResponse.getArtists().get(0).getName(),
                itemResponse.getName(),
                itemResponse.getName(),
                itemResponse.getExternalUrls().getSpotify(),
                name());
    }

    @Override
    public String name() {
        return API_EXTERNAL_NAME;
    }
}
