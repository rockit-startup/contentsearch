package com.quovadis.contentsearch.external.facade.tiktok.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
public class MediaTiktok {
    @JsonProperty("video_id")
    String videoId;

    @JsonProperty("create_time")
    Long createTime;

    @JsonProperty("description")
    String description;

    @JsonProperty("author")
    Author author;

    Statistics statistics;

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Statistics getStatistics() {
        return statistics;
    }

    public void setStatistics(Statistics statistics) {
        this.statistics = statistics;
    }

    @Override
    public String toString() {
        return "Media{" +
                "videoId='" + videoId + '\'' +
                ", createTime=" + createTime +
                ", description='" + description + '\'' +
                ", author=" + author +
                ", statistics=" + statistics +
                '}';
    }
}
