package com.quovadis.contentsearch.external.facade.tiktok.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.List;

@JsonAutoDetect
public class AllContentResponse {
    @JsonProperty("has_more")
    boolean hasMore;

    @JsonProperty("max_cursor")
    int maxCursor;

    @JsonProperty("media")
    List<MediaTiktok> mediaTiktokList;


    public boolean isHasMore() {
        return hasMore;
    }

    public void setHasMore(boolean hasMore) {
        this.hasMore = hasMore;
    }

    public int getMaxCursor() {
        return maxCursor;
    }

    public void setMaxCursor(int maxCursor) {
        this.maxCursor = maxCursor;
    }

    public List<MediaTiktok> getMediaList() {
        return mediaTiktokList;
    }

    public void setMediaList(List<MediaTiktok> mediaTiktokList) {
        this.mediaTiktokList = mediaTiktokList;
    }

    @Override
    public String toString() {
        return "AllContentResponse{" +
                "hasMore=" + hasMore +
                ", maxCursor=" + maxCursor +
                ", mediaList=" + mediaTiktokList +
                '}';
    }
}
