package com.quovadis.contentsearch.external.facade.youtube;

import com.quovadis.contentsearch.external.configuration.PropertyApiExternal;
import com.quovadis.contentsearch.external.facade.ApiExternalContentProvider;
import com.quovadis.contentsearch.external.facade.youtube.model.AllContentResponse;
import com.quovadis.contentsearch.external.facade.youtube.model.Item;
import com.quovadis.contentsearch.external.model.ContentApiExternalResponse;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.stream.Collectors;

public class YoutubeExternalContentWorker extends ApiExternalContentProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(YoutubeExternalContentWorker.class);

    final String API_EXTERNAL_NAME = "youtube";
    final String HEADER_AUTHORIZATION = "Authorization";

    @Autowired
    PropertyApiExternal propertyApiExternal;

    WebClient webClient;

    public YoutubeExternalContentWorker(){
        super();
    }

    @Override
    public void setUp() {
        this.webClient = webClient
                .mutate()
                .baseUrl(propertyApiExternal.getYoutubeBaseUrl())
                .defaultHeader(HEADER_AUTHORIZATION, propertyApiExternal.getYoutubeBearerToken())
                .build();
    }

    @Override
    public Flux<ContentApiExternalResponse> get(String param) {
        LOGGER.info("YoutubeExternalContentWorker get()");
        String searchUri = propertyApiExternal.getYoutubeSearchUri();
        searchUri = searchUri.concat("key=AIzaSyDpXYrednTAJwWqA5hduQgvh91WDfCrAaM");
        searchUri = searchUri.concat("&part=id,snippet");
        searchUri = searchUri.concat("&maxResults=10");
        searchUri = searchUri.concat("&q=").concat(param);

        LOGGER.info("YoutubeExternalContentWorker get(): {}", searchUri);

        Flux<ContentApiExternalResponse> response = (Flux<ContentApiExternalResponse>) webClient
                .method(HttpMethod.GET)
                .uri(searchUri)
                .retrieve()
                .bodyToMono(AllContentResponse.class)
                .onErrorReturn(new AllContentResponse())
                .flatMapMany(this::convertMonoToFlux)
                .delayElements(Duration.ofMillis(15l));

        return response;
    }

    private Publisher<?> convertMonoToFlux(AllContentResponse allContentResponse) {
        return Flux
                .fromIterable(allContentResponse
                        .getItems()
                        .stream()
                        .filter(item -> {
                            return item.getId().getVideoId() != null;
                        })
                        .map(this::convertToContentApiExternal)
                        .collect(Collectors.toList())
                );
    }

    private ContentApiExternalResponse convertToContentApiExternal(Item item) {
        return new ContentApiExternalResponse(
                item.getSnippet().getChannelTitle(),
                item.getSnippet().getTitle(),
                item.getSnippet().getDescription(),
                propertyApiExternal.getYoutubeWatch().concat(item.getId().getVideoId()),
                name());
    }

    @Override
    public String name() {
        return API_EXTERNAL_NAME;
    }
}
