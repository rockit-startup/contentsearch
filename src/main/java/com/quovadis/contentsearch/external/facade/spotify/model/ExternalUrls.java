package com.quovadis.contentsearch.external.facade.spotify.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
public class ExternalUrls {
    @JsonProperty("spotify")
    String spotify;

    public String getSpotify() {
        return spotify;
    }

    public void setSpotify(String spotify) {
        this.spotify = spotify;
    }

    @Override
    public String toString() {
        return "ExternalUrls{" +
                "spotify='" + spotify + '\'' +
                '}';
    }
}
