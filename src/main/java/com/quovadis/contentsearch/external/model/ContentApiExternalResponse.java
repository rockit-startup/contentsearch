package com.quovadis.contentsearch.external.model;

public class ContentApiExternalResponse {
    String author;
    String title;
    String description;
    String url;
    String apiExternal;

    public ContentApiExternalResponse(String author, String title, String description, String url, String apiExternal) {
        this.author = author;
        this.title = title;
        this.description = description;
        this.url = url;
        this.apiExternal = apiExternal;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getApiExternal() {
        return apiExternal;
    }

    public void setApiExternal(String apiExternal) {
        this.apiExternal = apiExternal;
    }

    @Override
    public String toString() {
        return "ContentApiExternalResponse{" +
                "author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", url='" + url + '\'' +
                ", apiExternal='" + apiExternal + '\'' +
                '}';
    }
}
