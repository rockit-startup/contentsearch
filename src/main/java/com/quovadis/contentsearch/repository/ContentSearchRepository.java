package com.quovadis.contentsearch.repository;

import com.quovadis.contentsearch.model.ContentDocument;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface ContentSearchRepository extends ReactiveCrudRepository<ContentDocument,String> {
    Flux<ContentDocument> findContentDocumentsByContentLike(String title);
}
