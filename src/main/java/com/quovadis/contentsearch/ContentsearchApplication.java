package com.quovadis.contentsearch;

import com.quovadis.contentsearch.model.ContentDocument;
import com.quovadis.contentsearch.repository.ContentSearchRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.ReactiveMongoOperations;
import reactor.core.publisher.Flux;

import java.util.Date;

@SpringBootApplication
public class ContentsearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContentsearchApplication.class, args);
	}

	//Keep the rest of the code untouched. Just add the following method
	@Bean
	CommandLineRunner init(ReactiveMongoOperations operations, ContentSearchRepository contentSearchRepository) {
		return args -> {
			contentSearchRepository
					.deleteAll()
					.thenMany(
							Flux.just(
									new ContentDocument("hawai","Hawái Local", "Maluma", "local", "local example", "", new Date()))
									.flatMap(contentSearchRepository::save))
					.thenMany(contentSearchRepository.findAll())
					.subscribe(System.out::println);
		};
	}
}
