package com.quovadis.contentsearch.service.imp;

import com.quovadis.contentsearch.external.service.ContentSearchExternalService;
import com.quovadis.contentsearch.model.ContentDocument;
import com.quovadis.contentsearch.repository.ContentSearchRepository;
import com.quovadis.contentsearch.service.ContentSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class ContentSearchServiceImp implements ContentSearchService {

    @Value("${response.code.quantity}")
    int responseCodeQuantity;

    @Autowired
    ContentSearchRepository contentSearchRepository;

    @Autowired
    ContentSearchExternalService contentSearchExternalService;

    @Override
    public Flux<ContentDocument> searchContent(String query) {
        return contentSearchRepository
                .findContentDocumentsByContentLike(query)
                .onErrorStop()
                .collectList()
                .flatMap(s -> s.size() >= responseCodeQuantity ? Mono.just(s) : concatInternalExternal(query))
                .flatMapMany(Flux::fromIterable);
    }

    private Mono<List<ContentDocument>> concatInternalExternal(String query){
        return contentSearchRepository
                .findContentDocumentsByContentLike(query)
                .mergeWith(contentSearchExternalService.findExternalContent(query)).collectList();
    }
}
