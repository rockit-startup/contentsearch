package com.quovadis.contentsearch.service;

import com.quovadis.contentsearch.model.ContentDocument;
import reactor.core.publisher.Flux;

public interface ContentSearchService {
    Flux<ContentDocument> searchContent(String query);
}
